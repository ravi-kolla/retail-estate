import React from 'react';
import {Route} from 'react-router-dom';
import Homepage from './components/pages/HomePage.js';
import ContactPage from './components/pages/ContactPage.js';
import Header from './components/nav/Header.js';
//import PropertyItem from './components/pages/PropertyItem.js';
import DetailPage from './components/pages/DetailPage.js';

const App = () =>(
   <div>
    <Header />
    <Route path='/' exact component={Homepage} />
    <Route path='/view/:id' exact component={DetailPage} />
    <Route path='/contact-us' exact component={ContactPage} />
    <Route path='/post-property' exact component={ContactPage} />
   </div>
)

export default App;
