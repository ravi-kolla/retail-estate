import React,{Component} from 'react';
//import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PropertyItem from './PropertyItem.js'

class PropertyGrid extends Component {
  render() {
  //  console.log("location:" + this.props.location);
    return(
      <div>
        <PropertyItem properties={this.props.properties} SelectedLocation={this.props.location} />
      </div>
   );
  }
}

function mapStateToProps(state){
  return{
    properties: state.properties.location,
    location: state.selectedLocation
  };
}
export default connect(mapStateToProps)(PropertyGrid);
