import React from 'react';
import ContactForm from '../forms/ContactForm.js'

const ContactPage = () =>(
   <div>
    <ContactForm />
   </div>
)

export default ContactPage;
