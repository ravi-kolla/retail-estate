import React,{Component} from 'react';
import {Link} from 'react-router-dom';
//import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Carousel from './CarouselComponent.js';
import { addDetail } from '../../actions/details.js';

class PropertyItem extends Component {
  constructor(){
    super();
  }


  render() {
    const area= this.props.SelectedLocation;
    const location= this.props.properties;
console.log("this.props.SelectedLocation:"+ this.props.SelectedLocation);
    return(
      <div className="row">
      {location[area].map((item, index) =>
      <div key={index} className="col-md-4 mt-3">
      <div className="card">
       <Carousel slides={item.images} />
       <div className="card-body">
         <h5 className="card-title">{item.title}</h5>
         <p className="card-text">{item.description}</p>
         <Link to={'/view/{{item.id}}'} onClick={() => addDetail(item)} className="btn btn-primary">Know more</Link>
       </div>
      </div>
      </div>
    )}
    </div>
   );
  }
}

const mapDispatchToProps = dispatch => ({
  addDetail: item => dispatch(addDetail(item))
})
export default connect(mapDispatchToProps)(PropertyItem);
