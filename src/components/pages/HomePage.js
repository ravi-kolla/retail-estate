import React,{ Component } from 'react';
//import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Carousel from './CarouselComponent.js';
import PropertyGrid from './PropertyGrid.js'
class Homepage extends Component {
  render() {
    //console.log("details:"+this.props.details);
    return(
      <div>
        <Carousel slides={this.props.slides.Hyderabad} />
        <div className="row">
        <div className="ml-3 col-md-9">
          <PropertyGrid />
        </div>

        </div>
      </div>
   );
  }
}



function mapStateToProps(state){
  return{
    slides: state.carousel.ads,
    property: state.properties.location,
    details: state.details.item,
    location: state.location
  };
}
export default connect(mapStateToProps)(Homepage);
