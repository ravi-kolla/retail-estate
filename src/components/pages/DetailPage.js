import React,{ Component } from 'react';
//import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
//import Carousel from './CarouselComponent.js';
import PropertyGrid from './PropertyGrid.js'
class DetailPage extends Component {
  render() {
    console.log(this.props.property);
    const i = 0;
    const item = this.props.property.AndhraPradesh[i];
    console.log(item);
    return(
      <div>
        <h1>{item.title}</h1>
        <h2>{item.contactName}</h2>
        <PropertyGrid />
      </div>
   );
  }
}



function mapStateToProps(state){
  return{
    slides: state.carousel.ads.builders,
    property: state.properties.location
  };
}
export default connect(mapStateToProps)(DetailPage);
