import React,{ Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {updateLocation} from '../../actions/locationAction.js';


class SelectLocation extends Component {
 constructor(props){
   super(props);
   this.Location = this.Location.bind(this);
  }

  Location(value){
    console.log(value);
  }
  render(){
    return(
      <select className="ui search dropdown" onChange={(e)=>this.props.updateLocation(e.target.value)}>
        <option value="Hyderabad">Hyderabad</option>
        <option value="Amaravathi">Amaravathi</option>
        <option value="Vijayawada">Vijayawada</option>
        <option value="Warangal">Warangal</option>
      </select>
   );
 }
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({updateLocation:updateLocation},dispatch);
}
export default connect(null,matchDispatchToProps)(SelectLocation);
