import React, { Component } from 'react';
import '../../bootstrap.min.css';
class CarouselLeftArrow extends Component {
  render() {
    return (
      <a
        className="carousel-control-prev"
        onClick={this.props.onClick}
      >
      <div className='carousel-btns'>
        <span className="carousel-control-prev-icon" aria-hidden="true" />
        <span className="sr-only">Previous</span>
       </div>
      </a>

    );
  }
}

class CarouselRightArrow extends Component {
  render() {
    return (
      <a
        className="carousel-control-next"
        onClick={this.props.onClick}
      >
      <div className='carousel-btns'>
        <span className="carousel-control-next-icon" aria-hidden="true" />
        <span className="sr-only">Next</span>
       </div>
      </a>
    );
  }
}

class CarouselSlide extends Component {
  render() {

    return (
      <div
        className={
          this.props.index === this.props.activeHero
            ? "carousel-item active"
            : "carousel-item"
        }
      >
        <img className="d-block w-100 carousel-height" alt="images" key={this.props.slide.id} src={this.props.slide.image} />
      </div>
    );
  }
}

 class Carousel extends Component {
  constructor(props){
    super(props);
    this.currentHero = this.currentHero.bind(this);
    this.goToPrevSlide = this.goToPrevSlide.bind(this);
    this.goToNextSlide = this.goToNextSlide.bind(this);
    this.state={ activeHero: 0};
  }
  currentHero(index) {
    this.setState({
      activeHero: index
    });
  }
  goToPrevSlide(e) {
    e.preventDefault();

    let index = this.state.activeHero;
    let { slides } = this.props;
    let slidesLength = slides.length;
    console.log({slidesLength});
    if (index < 1) {
      index = slidesLength;
    }

    --index;

    this.setState({
      activeHero: index
    });
  }

  goToNextSlide(e) {
    e.preventDefault();
  let index = this.state.activeHero;
    let { slides } = this.props;
    let slidesLength = slides.length - 1;

    if (index === slidesLength) {
      index = -1;
    }

    ++index;

    this.setState({
      activeHero: index
    });
  }
  render() {
    const area= this.props.SelectedArea;
    const location= this.props.slides;
  //  console.log("carousellocationcar:"+this.props.slides);
  //  console.log("carouselarea:"+this.props.SelectedArea);
    return (
     <div id="carouselExampleControls" className="carousel slide">
      <div className="carousel-inner">
        {this.props.slides.map((slide, index) =>
            <CarouselSlide
              key={index}
              index={index}
              activeHero={this.state.activeHero}
              slide={slide}
            />
          )}
      </div>
      <CarouselLeftArrow onClick={e => this.goToPrevSlide(e)} />
      <CarouselRightArrow onClick={e => this.goToNextSlide(e)}/>
      </div>
    );
  }
}

export default Carousel;
