import React, {Component} from 'react';

class ContactForm extends Component {

  constructor(props){
		super(props);
		this.state={data:{name:'',email:'',phone:'',text:''}}
	};

  onChange = e =>
  this.setState({
    data: {...this.state.data, [e.target.name]:e.target.value}
  })

  render(){
    const {data} = this.state;
    return (
      <form className="ui container form">
        <div className="required four wide field">
            <label>Name</label>
            <input
             placeholder="Name"
             value={data.name}
             type="text"
             name="name"
             onChange={this.onChange}
            />
        </div>
        <div className="required four wide field">
            <label>E-mail</label>
            <input
             type="email"
             name="email"
             placeholder="joe@schmoe.com"
             value={data.email}
            />
       </div>
       <div className="field four wide">
            <label>Phone Number</label>
            <input
               type="text"
               name="phone"
               placeholder="(xxx)"
               value={data.phone}
              />
        </div>
        <div className="field six wide">
          <label>Message</label>
          <textarea name="text" value={data.text}></textarea>
        </div>
        <div className="ui submit primary button">Submit</div>
      </form>
    )
  }
}
export default ContactForm;
