import React from 'react';
import {NavLink} from 'react-router-dom';
import SelectLocation from '../pages/SelectLocation.js';

const Header = () => {
   return (
     <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <NavLink className="navbar-brand" to="/">Logo</NavLink>
        <button className="navbar-toggler" type="button">
           <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div className="navbar-nav">
            <NavLink className="nav-item nav-link text-primary active" to="/">Home<span className="sr-only">(current)</span></NavLink>
            <NavLink className="nav-item nav-link text-primary" to="/contact-us">Contact Us</NavLink>
            <NavLink className="nav-item nav-link text-primary" to="/post-property">List your property</NavLink>
          </div>
          <div className="nav navbar-nav navbar-right">
            <SelectLocation />
          </div>
        </div>
      </nav>
    );
}

export default Header;
