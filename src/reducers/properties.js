import propertyData from '../data/propertyData.json';

function properties (){
  return propertyData;
}

export default properties;
