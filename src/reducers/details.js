const details = (state = {}, action) => {
  switch (action.type) {
    case 'FETCH_DETAIL':
      return action.item

    default:
      return state
  }
}
export default details;
