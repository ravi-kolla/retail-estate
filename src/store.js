import {createStore} from 'redux';
import rootReducer from './rootReducer';
import details from './reducers/details.js';
import carousel from './reducers/carousel.js';
import properties from './reducers/properties.js';
import selectedLocation from './reducers/location.js';

const defaultStore = {
  carousel,
  properties,
  details,
  selectedLocation
};

const store = createStore(rootReducer, defaultStore,window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;
