import {combineReducers} from 'redux';
import details from './reducers/details.js';
import carousel from './reducers/carousel.js';
import properties from './reducers/properties.js';
import selectedLocation from './reducers/location.js';
export default combineReducers({
  carousel,
  properties,
  details,
  selectedLocation
});
